module.exports = {
    /**
     * Removes everything but letters of the param.
     * 
     * @param {String} string
     * 
     * @returns {String}
     */
    Letters: function (string) {
        return string.replace(/[^a-zA-Z]/g, "");
    },

    /**
     * Removes everything but letters and spaces of the param.
     * 
     * @param {String} string
     * 
     * @returns {String}
     */
    LettersAndSpaces: function (string) {
        return string.replace(/[^a-zA-Z ]/g, "");
    },

    /**
     * Removes everything but letters and hyphens of the param.
     * 
     * @param {String} string
     * 
     * @returns {String}
     */
    LettersAndHyphens: function (string) {
        return string.replace(/[^a-zA-Z\-]/g, "");
    },

    /**
     * Removes everything but numbers of the param.
     * 
     * @param {String} string
     * 
     * @returns {String|Number}
     */
    Numbers: function (string) {
        return string.replace(/[^0-9]/g, "");
    },
    /**
     * Removes everything but letters and numbers of the param.
     * 
     * @param {String} string
     * 
     * @returns {String|Number}
     */
    LettersAndNumbers: function (string) {
        return string.replace(/[^a-zA-Z0-9]/g, "");
    },
    /**
     * Removes everything but Hexadecimal characters and hyphens.
     * 
     * @param {String} string
     * 
     * @returns {String}
     */
    HexadecimalAndHyphens: function (string) {
        return string.replace(/[^a-fA-F0-9\-]/g, "");
    },
    /**
     * Removes everything but letters, numbers, -, @, \ and _ of the param.
     * 
     * @param {String} string
     * 
     * @returns {String}
     */
    Services: function (string) {
        return string.replace(/[^a-zA-Z0-9\-@\\_]/g, "");
    },

    /**
     * Removes specified chars of the param.
     * 
     * @param {String} string
     * @param {String[]} charsToRemove Array of char or part of regex
     */
    Custom: function (string, charsToRemove) {
        var rs = "[";
        charsToRemove.forEach(char => {
            if (char == "\\") {
                rs += char;
            }
            rs += char;
        });
        rs += "]";
        var r = new RegExp(rs, "g");
        return string.replace(r, "");
    }
};