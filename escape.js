module.exports = {
    /**
     * Escapes spaces of the param.
     * 
     * @param {String} string
     * 
     * @returns {String}
     */
    Spaces: function (string) {
        return string.replace(/ /g, "\\ ");
    },

    /**
     * Escape quotes of the param.
     * 
     * @param {String} string
     * 
     * @returns {String}
     */
    Quotes: function (string) {
        return string.replace(/'/g, "\\'");
    },
    /**
     * Escape double quotes of the param.
     * 
     * @param {String} string
     * 
     * @returns {String}
     */
    DoubleQuotes: function (string) {
        return string.replace(/"/g, '\\"');
    },
    /**
     * Escape backslashes of the param.
     * 
     * @param {String} string
     * 
     * @returns {String}
     */
    Backslash: function (string) {
        return string.replace(/\\/g, '\\\\');
    }
};