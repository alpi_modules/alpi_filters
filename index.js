module.exports = {
    Escape: require('./escape'),
    KeepOnly: require('./keepOnly'),
    Validate: require('./validate'),
    Other: require('./other')
};