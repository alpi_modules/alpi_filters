module.exports = {
    /**
     * Normalizes the param as a path using the path module.
     * 
     * @param {String} string
     * 
     * @returns {String}
     */
    NormalizePath: function (string) {
        return require("path").normalize(string);
    },

    /**
     * Transforms the param in boolean.
     * 
     * @param {String|Number} string
     * 
     * @returns {Boolean}
     */
    ToBoolean: function (string) {
        return (string === "true") ? true : (string === "1") ? true : (string === 1) ? true : false;
    }
};